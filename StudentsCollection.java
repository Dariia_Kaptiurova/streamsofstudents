import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentsCollection {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<Student>();

        students.add(new Student(001, "Dariia", "Kaptiurova", "Oleksandrivna",
                2000, "Kyiv", "0972891086", "Fashion industry", "IV", "BVsk-19"));
        students.add(new Student(002, "Dariia", "Magdenko", "Oleksandrivna",
                2000, "Kovel", "0982891086", "Applied mathematic", "V", "FPM-18"));
        students.add(new Student(003, "Igor", "Magdenko", "Gennadiiovych",
                1993, "Kovel", "0672891086", "Applied mathematic", "IV", "FPM-18"));
        students.add(new Student(004, "Igor", "Solonina", "Gennadiiovych",
                1999, "Kyiv", "0972891086", "Medical", "III", "DM-17"));
        students.add(new Student(005, "Vadim", "Solonina", "Vitaliiovych",
                1998, "Dnipro", "0972991086", "Medical", "I", "DM-14"));
        students.add(new Student(006, "Elizaveta", "Solonina", "Vitaliivna",
                1998, "Dnipro", "0972851086", "Fashion industry", "IV", "BVsk-19"));
        students.add(new Student(007, "Olena", "Akopova", "Mychailivna",
                1999, "Kyiv", "0972898086", "Fashion industry", "III", "BVsk-19"));

        for (Student student : students) {
            System.out.println(student.toString());
        }
    }


    public static List<Student> studentsOnFaculty(List<Student> students, String faculty) {
        List<Student> allStudentsOnFaculty = students.stream().filter((a) -> a.getFaculty().equals(faculty)).collect(Collectors.toList());
        return allStudentsOnFaculty;
    }

    public static List<Student> studentsOnFacultyAndCourse(List<Student> students, String faculty, String course) {
        List<Student> allStudentsInFacultyAndCourse = students.stream().filter((a) -> a.getFaculty().equals(faculty) && a.getCourse().equals(course)).collect(Collectors.toList());
        return allStudentsInFacultyAndCourse;
    }

    public static List<Student> studentsAfterYearOfBirth(List<Student> students, int yearOfBirth) {
        List<Student> allStudentsAfterYearOfBirth = students.stream().filter((a) -> a.getYearOfBirth() >= yearOfBirth).collect(Collectors.toList());
        return allStudentsAfterYearOfBirth;
    }

    public static List<String> studentsInGroup(List<Student> students, String group) {
        List<String> groupOfStudents = students.stream()
                .filter((a) -> a.getGroup().equals(group))
                .map(a -> a.getLastName() + ", "
                        + a.getFirstName() + ", "
                        + a.getPatronymic())
                .collect(Collectors.toList());


        return groupOfStudents;
    }

    public static List<String> outputListOfStudents(List<Student> students) {
        List<String> groupOfStudents = students.stream()
                .map(a -> a.getLastName() + ", "
                        + a.getFirstName() + ", "
                        + a.getPatronymic() + " - "
                        + a.getFaculty() + ", "
                        + a.getGroup())
                .collect(Collectors.toList());

        return groupOfStudents;
    }

    public static long countStudentsOnFaculty(List<Student> students, String faculty) {
        return students.stream()
                .filter(a -> faculty == a.getFaculty())
                .count();
    }
}




