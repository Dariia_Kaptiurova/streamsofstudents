import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class StudentsCollectionTest {

    ArrayList<Student> test = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        test.add(new Student(001, "Dariia", "Kaptiurova", "Oleksandrivna",
                2000, "Kyiv", "0972891086", "Fashion industry", "IV", "BVsk-19"));
        test.add(new Student(002, "Dariia", "Magdenko", "Oleksandrivna",
                2000, "Kovel", "0982891086", "Applied mathematic", "V", "FPM-18"));
        test.add(new Student(003, "Igor", "Magdenko", "Gennadiiovych",
                1993, "Kovel", "0672891086", "Applied mathematic", "IV", "FPM-18"));
        test.add(new Student(004, "Igor", "Solonina", "Gennadiiovych",
                1999, "Kyiv", "0972891086", "Medical", "III", "DM-17"));
        test.add(new Student(005, "Vadim", "Solonina", "Vitaliiovych",
                1998, "Dnipro", "0972991086", "Medical", "I", "DM-14"));
        test.add(new Student(006, "Elizaveta", "Solonina", "Vitaliivna",
                1998, "Dnipro", "0972851086", "Fashion industry", "IV", "BVsk-19"));
        test.add(new Student(007, "Olena", "Akopova", "Mychailivna",
                1999, "Kyiv", "0972898086", "Fashion industry", "III", "BVsk-19"));
    }

    @Test
    public void shouldGetStudentsOnFaculty() {

        //given
        String faculty = "Applied mathematic";

        //when
        String expected = "[\n" +
                "id: 2, \n" +
                "lastName: Magdenko, \n" +
                "firstName: Dariia, \n" +
                "patronymic: Oleksandrivna, \n" +
                "yearOfBirth: 2000, \n" +
                "address: Kovel, \n" +
                "telephone: 0982891086, \n" +
                "faculty: Applied mathematic, \n" +
                "course: V, \n" +
                "group: FPM-18 \n" +
                ", \n" +
                "id: 3, \n" +
                "lastName: Magdenko, \n" +
                "firstName: Igor, \n" +
                "patronymic: Gennadiiovych, \n" +
                "yearOfBirth: 1993, \n" +
                "address: Kovel, \n" +
                "telephone: 0672891086, \n" +
                "faculty: Applied mathematic, \n" +
                "course: IV, \n" +
                "group: FPM-18 \n" +
                "]";
        List<Student> actual = StudentsCollection.studentsOnFaculty(test, faculty);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsOnFacultyAndCourse() {

        //given
        String faculty = "Fashion industry";
        String course = "IV";

        //when
        String expected = "[\n" +
                "id: 1, \n" +
                "lastName: Kaptiurova, \n" +
                "firstName: Dariia, \n" +
                "patronymic: Oleksandrivna, \n" +
                "yearOfBirth: 2000, \n" +
                "address: Kyiv, \n" +
                "telephone: 0972891086, \n" +
                "faculty: Fashion industry, \n" +
                "course: IV, \n" +
                "group: BVsk-19 \n" +
                ", \n" +
                "id: 6, \n" +
                "lastName: Solonina, \n" +
                "firstName: Elizaveta, \n" +
                "patronymic: Vitaliivna, \n" +
                "yearOfBirth: 1998, \n" +
                "address: Dnipro, \n" +
                "telephone: 0972851086, \n" +
                "faculty: Fashion industry, \n" +
                "course: IV, \n" +
                "group: BVsk-19 \n" +
                "]";
        List<Student> actual = StudentsCollection.studentsOnFacultyAndCourse(test, faculty, course);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsAfterYearOfBirth() {

        //given
        int yearOfBirth = 1999;

        //when
        String expected = "[\n" +
                "id: 1, \n" +
                "lastName: Kaptiurova, \n" +
                "firstName: Dariia, \n" +
                "patronymic: Oleksandrivna, \n" +
                "yearOfBirth: 2000, \n" +
                "address: Kyiv, \n" +
                "telephone: 0972891086, \n" +
                "faculty: Fashion industry, \n" +
                "course: IV, \n" +
                "group: BVsk-19 \n" +
                ", \n" +
                "id: 2, \n" +
                "lastName: Magdenko, \n" +
                "firstName: Dariia, \n" +
                "patronymic: Oleksandrivna, \n" +
                "yearOfBirth: 2000, \n" +
                "address: Kovel, \n" +
                "telephone: 0982891086, \n" +
                "faculty: Applied mathematic, \n" +
                "course: V, \n" +
                "group: FPM-18 \n" +
                ", \n" +
                "id: 4, \n" +
                "lastName: Solonina, \n" +
                "firstName: Igor, \n" +
                "patronymic: Gennadiiovych, \n" +
                "yearOfBirth: 1999, \n" +
                "address: Kyiv, \n" +
                "telephone: 0972891086, \n" +
                "faculty: Medical, \n" +
                "course: III, \n" +
                "group: DM-17 \n" +
                ", \n" +
                "id: 7, \n" +
                "lastName: Akopova, \n" +
                "firstName: Olena, \n" +
                "patronymic: Mychailivna, \n" +
                "yearOfBirth: 1999, \n" +
                "address: Kyiv, \n" +
                "telephone: 0972898086, \n" +
                "faculty: Fashion industry, \n" +
                "course: III, \n" +
                "group: BVsk-19 \n" +
                "]";
        List<Student> actual = StudentsCollection.studentsAfterYearOfBirth(test, yearOfBirth);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsInGroup() {

        //given
        String group = "BVsk-19";

        //when
        String expected = "[Kaptiurova, Dariia, Oleksandrivna, Solonina, Elizaveta, Vitaliivna, Akopova, Olena, Mychailivna]";
        List<String> actual = StudentsCollection.studentsInGroup(test, group);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldOutputListOfStudents() {

        //given

        //when
        String expected = "[Kaptiurova, Dariia, Oleksandrivna - Fashion industry, BVsk-19, Magdenko, Dariia, Oleksandrivna - Applied mathematic, FPM-18, Magdenko, Igor, Gennadiiovych - Applied mathematic, FPM-18, Solonina, Igor, Gennadiiovych - Medical, DM-17, Solonina, Vadim, Vitaliiovych - Medical, DM-14, Solonina, Elizaveta, Vitaliivna - Fashion industry, BVsk-19, Akopova, Olena, Mychailivna - Fashion industry, BVsk-19]";
        List<String> actual = StudentsCollection.outputListOfStudents(test);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldCountStudentsOnFaculty() {

        //given
        String faculty = "Fashion industry";

        //when
        long expected = 3;
        long actual = StudentsCollection.countStudentsOnFaculty(test, faculty);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }
}